#include <iostream>
#include <memory>
#include "logger.hpp"
#include "user_data_reader.hpp"
#include "local_matrices_generator.hpp"
#include "element_generator.hpp"
#include "file_reader.hpp"
#include "global_matrix_generator.hpp"
#include "boost/filesystem.hpp"

#include <armadillo>

#include <vector>

namespace fs = boost::filesystem;
using namespace arma;

int main() {
    auto constants = std::make_unique<Constants>();
    fs::path path = "/home/oskar/CLionProjects/mes/data.txt";
    ReadDataFromFile(path, constants);
    ReadNumberOfNodesFromUser(constants);
    PrintData(constants);

    auto elements = GenerateElements(constants);
    GenerateLocalMatrices(elements, constants);

    auto global_matrix_HG = GenerateGlobalMatrixHG(elements);
    auto global_matrix_PG = GenerateGlobalMatrixPG(elements);
    PrintGlobalMatrixHG(global_matrix_HG);
    PrintGlobalMatrixPG(global_matrix_PG);

    //converting matrices to armadillo matrices
    mat global_hg_matrix_generated_by_arma(global_matrix_HG.size(), global_matrix_HG.size(), fill::zeros);
    mat global_pg_matrix_generated_by_arma(global_matrix_PG.size(), 1, fill::zeros);

    for(int i=0; i<global_matrix_HG.size(); i++) {
        for(int j=0; j<global_matrix_HG.size(); j++) {
            global_hg_matrix_generated_by_arma.at(i,j) = global_matrix_HG.at(i).at(j);
        }
    }

    for(int i=0; i<global_matrix_PG.size(); i++) {
        global_pg_matrix_generated_by_arma.at(i) = global_matrix_PG.at(i);
    }


    std::cout << "*************************************" << std::endl;
    global_hg_matrix_generated_by_arma.print();
    std::cout << "*************************************" << std::endl;
    global_pg_matrix_generated_by_arma.print();
    std::cout << "*************************************" << std::endl;

    mat result = global_hg_matrix_generated_by_arma * global_pg_matrix_generated_by_arma;

    //mat result = solve(global_hg_matrix_generated_by_arma, global_pg_matrix_generated_by_arma);


    return 0;
}
