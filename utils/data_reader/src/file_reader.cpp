#include "file_reader.hpp"
#include <fstream>
#include <iostream>
#include "logger.hpp"
#include <boost/lexical_cast.hpp>

using namespace boost;

void ReadDataFromFile(fs::path &path, std::unique_ptr<Constants> &data) {

    MES_LOG_INFO("Reading data from file");
    std::string data_from_file[6];
    std::string line;
    std::ifstream file(path.string());

    if (file.is_open()){
        int i = 0;
        while ( getline (file,line) ) {
            int equal_sign_position = static_cast<int>(line.find('='));
            data_from_file[i] = line.substr(static_cast<unsigned int>(equal_sign_position)+1);
            data_from_file[i].pop_back(); //TODO: Refactor needed
            i++;
        }
        file.close();
    } else {
        MES_LOG_ERROR("Coudn't open file");
    }

    try {
        data->area = lexical_cast<double>(data_from_file[0]);
        data->length = lexical_cast<double>(data_from_file[1]);
        data->convection_factor = lexical_cast<double>(data_from_file[2]);
        data->ambient_temperature = lexical_cast<double>(data_from_file[3]);
        data->heat_transfer_coefficient = lexical_cast<double>(data_from_file[4]);
        data->heat_flux = lexical_cast<double>(data_from_file[5]);
    } catch (bad_lexical_cast &e) {
        MES_LOG_ERROR("Wrong data format");
    }

    MES_LOG_INFO("SUCCESS");

}