#pragma once
#include "string"
#include "memory"
#include "constants.hpp"
#include "boost/filesystem.hpp"

namespace fs = boost::filesystem;

void ReadDataFromFile(fs::path &path, std::unique_ptr<Constants> &data);