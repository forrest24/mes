#include <iostream>
#include "element.hpp"
#include "local_matrices_generator.hpp"
#include "logger.hpp"


void GenerateLocalMatrices(std::vector<Element> &elements, std::unique_ptr<Constants> &constants) {

    MES_LOG_INFO("Generating local matrices for each element");
    for (auto &element : elements) {

        double C_positive = element.area*element.convection_factor/element.length;
        double C_negative = -(element.area*element.convection_factor/element.length);

        //Points are indices in local matrix, first two values are refering to indices in global matrix
        auto point_1_HG = std::make_tuple(element.id, element.id, C_positive);
        auto point_2_HG = std::make_tuple(element.id, element.id+1, C_negative);
        auto point_3_HG = std::make_tuple(element.id+1, element.id, C_negative);

        auto point_4_HG = std::make_tuple(element.id+1, element.id+1,
                                       element.is_end_point ? C_positive + constants->ambient_temperature * element.area : C_positive);

        auto point_2_PG = std::make_pair(constants->number_of_nodes-1, element.is_start_point ? -(constants->heat_flux * constants->ambient_temperature * constants->area) : 0);
        auto point_1_PG = std::make_pair(0, element.is_end_point ? constants->heat_transfer_coefficient * constants->area : 0);

        element.local_matrix_HG.push_back(point_1_HG);
        element.local_matrix_HG.push_back(point_2_HG);
        element.local_matrix_HG.push_back(point_3_HG);
        element.local_matrix_HG.push_back(point_4_HG);

        element.local_matrix_PG.push_back(point_1_PG);
        element.local_matrix_PG.push_back(point_2_PG);
    }
    MES_LOG_INFO("SUCCESS");

}
