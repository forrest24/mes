#pragma once

#include <vector>
#include <element.hpp>
#include <constants.hpp>
#include <memory>

void GenerateLocalMatrices(std::vector<Element> &elements, std::unique_ptr<Constants> &constants);