#pragma once

#include <vector>
#include <element.hpp>

std::vector<std::vector<double>> GenerateGlobalMatrixHG(std::vector<Element> elements);
std::vector<double> GenerateGlobalMatrixPG(std::vector<Element> elements);

