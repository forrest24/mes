#include <iostream>
#include "global_matrix_generator.hpp"
#include "logger.hpp"

std::vector<std::vector<double>> GenerateGlobalMatrixHG(std::vector<Element> elements) {
    MES_LOG_INFO("Generating global matrix HG");
    auto global_matrix_size = static_cast<unsigned int>(elements.size() + 1);
    std::vector<std::vector<double>> global_matrix(global_matrix_size);

    for(auto &v : global_matrix)
        v = std::vector<double>(global_matrix_size);

    for(const auto &element : elements) {
        for(auto point : element.local_matrix_HG) {
            auto x = static_cast<unsigned int>(std::get<0>(point));
            auto y = static_cast<unsigned int>(std::get<1>(point));
            auto value = std::get<2>(point);

            global_matrix.at(x).at(y) += value;
        }
    }
    MES_LOG_INFO("SUCCESS");
    return global_matrix;
}

std::vector<double> GenerateGlobalMatrixPG(std::vector<Element> elements) {
    MES_LOG_INFO("Generating global matrix PG");
    auto global_matrix_size = static_cast<unsigned int>(elements.size() + 1);
    std::vector<double> global_matrix(global_matrix_size);

    std::fill(global_matrix.begin(), global_matrix.end(), 0);

    for(const auto &element : elements) {
        for(auto point : element.local_matrix_PG) {
            auto index = static_cast<unsigned int>(std::get<0>(point));
            auto value = std::get<1>(point);

            global_matrix.at(index) += value;
        }
    }

    MES_LOG_INFO("SUCCESS");
    return global_matrix;
}
