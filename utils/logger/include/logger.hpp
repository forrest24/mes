#pragma once
#include <string>
#include <vector>
#include <memory>
#include <constants.hpp>

void MES_LOG_INFO(std::string message);
void MES_LOG_ERROR(std::string message);
void PrintGlobalMatrixHG(std::vector<std::vector<double>> global_matrix);
void PrintGlobalMatrixPG(std::vector<double> global_matrix);
void PrintData(std::unique_ptr<Constants> &data);
