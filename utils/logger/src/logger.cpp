#include "logger.hpp"
#include <string>
#include <iostream>

#include "boost/optional.hpp"

void MES_LOG_INFO(std::string message) {
    std::cout << "INFO: " << message << std::endl;
}

void MES_LOG_ERROR(std::string message) {
    std::cout << "ERROR: " << message << std::endl;
}

void PrintGlobalMatrixHG(std::vector<std::vector<double>> global_matrix) {
    std::cout << "****** Generated global matrix HG ******" << std::endl;

    for(auto &v : global_matrix) {
        for(auto &v2 : v)
            std::cout << v2 << " ";
        std::cout << std::endl;
    }
}

void PrintGlobalMatrixPG(std::vector<double> global_matrix) {
    std::cout << "****** Generated global matrix PG******" << std::endl;

    for(auto &v : global_matrix) {
        std::cout << v << std::endl;
    }
}

void PrintData(std::unique_ptr<Constants> &data) {
    std::cout << "****** Data from file ******" << std::endl;
    std::cout << "Area: " << data->area << std::endl;
    std::cout << "Length: " << data->length << std::endl;
    std::cout << "Convection_factor: " << data->convection_factor << std::endl;
    std::cout << "Ambient_temperature: " << data->ambient_temperature << std::endl;
    std::cout << "Heat_transfer_coefficient: " << data->heat_transfer_coefficient << std::endl;
    std::cout << "Number_of_nodes: " << data->number_of_nodes << std::endl;
    std::cout << "Heat_flux: " << data->heat_flux << std::endl;
}
