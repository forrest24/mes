#include <memory>
#include <vector>
#include <iostream>
#include "logger.hpp"
#include "element_generator.hpp"

std::vector<Element> GenerateElements(std::unique_ptr<Constants> &data) {

    MES_LOG_INFO("Generating elements");
    int number_of_elements = data->number_of_nodes-1;
    std::vector<Element> elements;

    for(int i=0; i<number_of_elements; i++) {
        Element element{};
        element.id = i;
        element.area = data->area;
        element.convection_factor = data->convection_factor;
        element.length = data->length / number_of_elements;
        element.start_index = i;
        element.end_index = element.start_index + 1;
        element.is_end_point = (i == number_of_elements - 1);
        element.is_start_point = (i == 0);

        elements.push_back(element);
    }
    MES_LOG_INFO("SUCCESS");
    return elements;
}
