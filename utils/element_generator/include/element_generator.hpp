#pragma once

#include <memory>
#include "constants.hpp"
#include "element.hpp"
#include "vector"

std::vector<Element> GenerateElements(std::unique_ptr<Constants> &data);