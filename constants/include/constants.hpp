#pragma once

struct Constants {
    double area;
    double length;
    double convection_factor;
    double ambient_temperature;
    double heat_transfer_coefficient;
    int number_of_nodes;
    double heat_flux;
};

