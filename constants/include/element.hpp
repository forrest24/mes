#pragma once

#include <vector>
#include <tuple>

struct Element {
    int id;
    int start_index;
    int end_index;
    double length;
    double area;
    double convection_factor;
    bool is_start_point;
    bool is_end_point;

    std::vector<std::tuple<int, int, double>> local_matrix_HG;
    std::vector<std::pair<int, double>> local_matrix_PG;
};
